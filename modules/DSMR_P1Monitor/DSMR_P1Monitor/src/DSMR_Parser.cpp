#include "DSMR_Parser.hpp"
#include <zephyr/sys/crc.h>
#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(dmsr_parser, LOG_LEVEL_DBG);


uint16_t DSMR::Parser::CalculateCrc(const uint8_t* data, const size_t& len){
    return crc16_reflect(0xA001,0x0000,data, len);
}

uint16_t DSMR::Parser::FetchCrc(const uint8_t* data, const size_t& len){
    const uint8_t *start = reinterpret_cast<const uint8_t*>(strrchr(reinterpret_cast<const char*>(data), '!'));
    if (start != NULL && (len - (start - data)) >= 5) {
        start++; // Move past the '!'
        uint16_t value = 0;
        
        for (int i = 0; i < 4; i++) {
            value = (value << 4) | (start[i] <= '9' ? (start[i] - '0') : ((start[i] & 0xdf) - 'A' + 10));
        }
        
        return value;

    } else {
        return 0; // Return 0 if the '!' is not found or there are less than 4 characters after '!'
    }
}

float DSMR::Parser::extractFloatValue(const char* input) {
    const char* start = strchr(input, '(');
    const char* end = strchr(input, '*'); // Assuming the '*' character denotes the end of the value

    if (start != NULL && end != NULL && start < end) {
        size_t length = end - (start + 1);
        char value[length+1]; 

        memcpy(value, start + 1, length);
        value[length] = '\0';

        float extractedValue = strtof(value, NULL); // Convert string to float
        return extractedValue;
    }

    return 0.0f; // Return default value if extraction fails
}

int DSMR::Parser::extractIntValue(const char* input) {
    const char* start = strchr(input, '(');
    const char* end = strchr(input, ')');

    if (start != NULL && end != NULL && start < end) {
        int value = 0;
        const char* p = start + 1; // Move past the '('
        
        while (p < end && *p >= '0' && *p <= '9') {
            value = value * 10 + (*p - '0');
            p++;
        }

        return value;
    }

    return 0; // Return default value if extraction fails
}

void DSMR::Parser::extractTimestamp(const char* input, OBISTimestamp& stamp) {
    if (input[9] != '(' || input[23] != ')') {
        // Handling invalid input string
        stamp.year = 0;
        stamp.month = 0;
        stamp.day = 0;
        stamp.hour = 0;
        stamp.minute = 0;
        stamp.second = 0;
        return;
    }

    char yearStr[3], monthStr[3], dayStr[3], hourStr[3], minuteStr[3], secondStr[3];
    memcpy(yearStr, input + 10, 2);
    yearStr[2] = '\0';
    memcpy(monthStr, input + 12, 2);
    monthStr[2] = '\0';
    memcpy(dayStr, input + 14, 2);
    dayStr[2] = '\0';
    memcpy(hourStr, input + 16, 2);
    hourStr[2] = '\0';
    memcpy(minuteStr, input + 18, 2);
    minuteStr[2] = '\0';
    memcpy(secondStr, input + 20, 2);
    secondStr[2] = '\0';

    stamp.year = atoi(yearStr);
    stamp.month = atoi(monthStr);
    stamp.day = atoi(dayStr);
    stamp.hour = atoi(hourStr);
    stamp.minute = atoi(minuteStr);
    stamp.second = atoi(secondStr);
}

void DSMR::Parser::extractSerialNumber(const char* input, char* sn){
    const char* start = strchr(input, '(');
    const char* end = strchr(input, ')'); // Assuming the '*' character denotes the end of the value

    if (start != NULL && end != NULL && start < end) {
        size_t length = end - (start + 1);
        memcpy(sn, start + 1, length);
        sn[length] = '\0';
    }

}

DSMR::OBIS_REF_IDX DSMR::Parser::FindIdx(const char* line) {
    size_t numberOfItems = sizeof(DSMR::obis_code_list) / sizeof(DSMR::obis_code_list[0]);

    for (size_t i = 0; i < numberOfItems; ++i) {
        if (strncmp(line, DSMR::obis_code_list[i].code, strlen(DSMR::obis_code_list[i].code)) == 0) {

            return DSMR::obis_code_list[i].idx;
        }
    }

    // Return a value indicating code not found
    LOG_ERR("OBIS code not found : %s", line);
    return OBIS_NOT_FOUND;
}

/**
 * @brief Helper function for the parsing of the 13 months obis code
 * 
 * @param start start address of the variable 
 * @param end end address of the variable
 * @return uint8_t char to int value
 */
static uint8_t convertCharsToUint8(const char *start, const char *end) {
    // Assuming that start and end point to a valid substring
    // If you want to perform additional validation, add appropriate checks here

    // Convert the substring to uint8_t
    uint8_t result = 0;
    while (start <= end) {
        // Assuming the characters between start and end are digits
        if (*start >= '0' && *start <= '9') {
            result = result * 10 + (*start - '0');
        }
        start++;
    }

    return result;
}

/**
 * @brief Helper function for the parsing of the 13 months obis code
 * 
 * @param start start address of the variable 
 * @param end end address of the variable
 * @return uint8_t char to float value
 */
float convertCharsToFloat(const char *start, const char *end) {
    // Assuming that start and end point to a valid substring
    // If you want to perform additional validation, add appropriate checks here

    // Convert the substring to float
    float result = 0.0f;
    float fraction = 0.1f;
    bool isFractionalPart = false;

    while (start <= end) {
        if (*start >= '0' && *start <= '9') {
            if (isFractionalPart) {
                result = result + (*start - '0') * fraction;
                fraction /= 10.0f;
            } else {
                result = result * 10.0f + (*start - '0');
            }
        } else if (*start == '.' && !isFractionalPart) {
            isFractionalPart = true;
        }
        start++;
    }

    return result;
}

void DSMR::Parser::extractLast13Months(const char *input, DSMR::OBISMaxDemand13Months &demand) {
    bool firstBracketFound{false};
    bool lastBracketFound{false};
    const char *start;
    const char *end;
    uint8_t index{0};

    const char *inputPtr = input;  // Use a separate pointer for iteration

    while (*inputPtr != '\r') {
        if (!firstBracketFound) {
            firstBracketFound = (*inputPtr == '(') ? true : false;
            start = inputPtr + 1;
        } else if (!lastBracketFound) {
            lastBracketFound = (*inputPtr == ')') ? true : false;
            end = inputPtr - 1;
            if (lastBracketFound) {
                demand.len = convertCharsToUint8(start, end);
            }
        } else {
            if (*inputPtr == '*') {
                demand.data[index++] = convertCharsToFloat(inputPtr - 6, inputPtr - 1);
            }
        }
        inputPtr++;
    }
}

bool DSMR::Parser::DataParse(const char* line, const OBIS_REF_IDX& idx){
    switch (idx) {
        case DSMR::OBIS_ID_IDX:
            mMeasurements.dev.ID = extractIntValue(line);
            // LOG_DBG("ID :%d", extractIntValue(line));
            break;
        case DSMR::OBIS_SN_IDX:
            extractSerialNumber(line, mMeasurements.dev.serialnumber);
            // LOG_DBG("SN :%d", extractIntValue(line));
            break;
        case DSMR::OBIS_TIMESTAMP_IDX:
            extractTimestamp(line, mMeasurements.stamp);
            // LOG_INFO("Time: %d/%d/20%d", t.day, t.month, t.year);
            break;
        case DSMR::OBIS_TOTAL_CONSUMPTION_RATE_1_IDX:
            mMeasurements.powers.consumptionRate1 = extractFloatValue(line);
            // LOG_DBG("Total Consumption 1: %f kW", extractFloatValue(line));
            break;
        case DSMR::OBIS_TOTAL_CONSUMPTION_RATE_2_IDX:
            mMeasurements.powers.consumptionRate2 = extractFloatValue(line);
            // LOG_DBG("Total Consumption 2: %f kW", extractFloatValue(line));
            break;
        case DSMR::OBIS_TOTAL_PRODUCTION_RATE_1_IDX:
            mMeasurements.powers.productionRate1 = extractFloatValue(line);
            // LOG_DBG("Total Production 1: %f kW", extractFloatValue(line));
            break;
        case DSMR::OBIS_TOTAL_PRODUCTION_RATE_2_IDX:
            mMeasurements.powers.productionRate2 = extractFloatValue(line);
            // LOG_DBG("Total Production 2: %f kW", extractFloatValue(line));
            break;
        case DSMR::OBIS_CURRENT_RATE_IDX:
            mMeasurements.dev.rate = extractIntValue(line);
            // LOG_DBG("Current rate:%s", extractIntValue(line)==1?"Day":"Night");
            break;
        case DSMR::OBIS_ALL_PHASE_CONSUMPTION_IDX:
            mMeasurements.consumptions.all= extractFloatValue(line);
            // LOG_DBG("All phase consumption:%f kW", extractFloatValue(line));
            break;
        case DSMR::OBIS_ALL_PHASE_PRODUCTION_IDX:
            mMeasurements.productions.all = extractFloatValue(line);
            // LOG_DBG("All phase production:%f kW", extractFloatValue(line));
            break;
        case DSMR::OBIS_L1_PHASE_CONSUMPTION_IDX:
            mMeasurements.consumptions.L1 = extractFloatValue(line);
            // LOG_DBG("L1 consumption:%f kW", extractFloatValue(line));
            break;
        case DSMR::OBIS_L2_PHASE_CONSUMPTION_IDX:
            mMeasurements.consumptions.L2 = extractFloatValue(line);
            // LOG_DBG("L2 consumption:%f kW", extractFloatValue(line));
            break;
        case DSMR::OBIS_L3_PHASE_CONSUMPTION_IDX:
            mMeasurements.consumptions.L3 = extractFloatValue(line);
            // LOG_DBG("L3 consumption:%f kW", extractFloatValue(line));
            break;
        case DSMR::OBIS_L1_PHASE_PRODUCTION_IDX:
            mMeasurements.productions.L1 = extractFloatValue(line);
            // LOG_DBG("L1 production:%f kW", extractFloatValue(line));
            break;
        case DSMR::OBIS_L2_PHASE_PRODUCTION_IDX:
            mMeasurements.productions.L2 = extractFloatValue(line);
            // LOG_DBG("L2 production:%f kW", extractFloatValue(line));
            break;
        case DSMR::OBIS_L3_PHASE_PRODUCTION_IDX:
            mMeasurements.productions.L3 = extractFloatValue(line);
            // LOG_DBG("L3 production:%f kW", extractFloatValue(line));
            break;
        case DSMR::OBIS_L1_VOLTAGE_IDX:
            mMeasurements.voltages.L1 = extractFloatValue(line);
            // LOG_DBG("L1 Volt:%f V", extractFloatValue(line));
            break;
        case DSMR::OBIS_L2_VOLTAGE_IDX:
            mMeasurements.voltages.L2 = extractFloatValue(line);
            // LOG_DBG("L2 Volt:%f V", extractFloatValue(line));
            break;
        case DSMR::OBIS_L3_VOLTAGE_IDX:
            mMeasurements.voltages.L3 = extractFloatValue(line);
            // LOG_DBG("L3 Volt:%f V", extractFloatValue(line));
            break;
        case DSMR::OBIS_L1_CURRENT_IDX:
            mMeasurements.currents.L1 = extractFloatValue(line);
            // LOG_DBG("L1 Current:%f A", extractFloatValue(line));
            break;
        case DSMR::OBIS_L2_CURRENT_IDX:
            mMeasurements.currents.L2 = extractFloatValue(line);
            // LOG_DBG("L2 Current:%f A", extractFloatValue(line));
            break;
        case DSMR::OBIS_L3_CURRENT_IDX:
            mMeasurements.currents.L3 = extractFloatValue(line);
            // LOG_DBG("L3 Current:%f A", extractFloatValue(line));
            break;
        case DSMR::OBIS_BREAKER_STATE_IDX:
            mMeasurements.dev.breaker = extractIntValue(line);
            // LOG_DBG("Breaker:%s", ? "ON":"OFF");
            break;
        case DSMR::OBIS_MAX_ALLOWED_POWER_IDX:
            mMeasurements.averages.maximum_allowed = extractFloatValue(line);
            // LOG_DBG("MAX power:%f kW", extractFloatValue(line));
            break;
        case DSMR::OBIS_MAX_ALLOWED_CURRENT_IDX:
            mMeasurements.currents.maximum_allowed = extractFloatValue(line);
            // LOG_DBG("Max current:%f A", extractFloatValue(line));
            break;
        case DSMR::OBIS_CURRENT_AVERAGE_IDX:
            mMeasurements.averages.current = extractFloatValue(line);
            // LOG_DBG("Current average:%f kW?", extractFloatValue(line));
            break;
        case DSMR::OBIS_MAX_CONSUMPTION_CURRENT_MONTH_IDX:
            mMeasurements.averages.maximum = extractFloatValue(&line[10]);
            // LOG_DBG("Max consumption:%f kW", extractFloatValue(&line[10]));
            break;
        case DSMR::OBIS_MAX_CONSUMPTION_LAST_13_MONTHS_IDX:
            // Not supported yet
            extractLast13Months(line, mMeasurements.averages.demand);
            break;
        case DSMR::OBIS_TEXT_MESSAGE_IDX:
            // Not supported yet
            break;
        default:
            LOG_ERR("Invalid Index to parse");
            return false;
            break;
    }
    return true;
}



bool DSMR::Parser::LineParse(const uint8_t* data, const size_t& len){
    size_t startIdx{0};
    uint8_t numberOfLines{0};
    if(data[0] != '/'){
        LOG_ERR("Did not receive full message.");
        return false;
    }
        
    for(size_t i = 4; i < len-2-4; i++){ // start add identification and ignore crc
        if(data[i] == '\n'){
            numberOfLines++;
            if(numberOfLines > 2)
                DataParse(reinterpret_cast<const char*>(&data[startIdx]), FindIdx(reinterpret_cast<const char*>(&data[startIdx]))) ;
            startIdx = i+1;
        }
    }
    return true;
}


bool DSMR::Parser::Parse(const uint8_t* data, const size_t& len){
    uint16_t calc_crc = CalculateCrc(data, len-2-4);
    uint16_t recv_crc = FetchCrc(data, len);
    // LOG_DBG("RCV CRC: 0x%x, CALC CRC: 0x%x", recv_crc, calc_crc);
    LineParse(data, len);
    return calc_crc == recv_crc;
}
