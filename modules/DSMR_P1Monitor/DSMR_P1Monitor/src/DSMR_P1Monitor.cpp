#include "DSMR_P1Monitor.hpp"
#include <string.h>

#include <zephyr/drivers/uart.h>
#include <zephyr/sys/printk.h>
#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(dmsr_monitor, LOG_LEVEL_DBG);

K_MSGQ_DEFINE(dsmr_rx_msgq, sizeof(DSMR::uart_dsmr_monitor_rx_buf), DSMR::P1Monitor::MSG_QUEUE_SIZE, 1);

void DSMR::uartReceiveTimeout(struct k_timer *timerId){
    ARG_UNUSED(timerId);
    auto& instance{DSMR::P1Monitor::GetInstance()};
    if (k_msgq_put(&dsmr_rx_msgq, &instance.mCurrentBuffer, K_NO_WAIT) != 0) {  // Put received data in queue
        /* message queue is full:*/
        LOG_WRN("(%s,%d) RX MSG Queue full!", __func__, __LINE__);
    }
    instance.mCurrentBuffer.length = 0; // Clear buffer
    memset(instance.mCurrentBuffer.data, 0, sizeof(instance.mCurrentBuffer.data));  // Clear buffer
}


void DSMR::threadSerialDataHandler(void *, void *, void *){
    auto& instance{DSMR::P1Monitor::GetInstance()};
    uart_dsmr_monitor_rx_buf rx_buf;
    for(;;) {
        /* get a data item */
        if(k_msgq_get(&dsmr_rx_msgq, &rx_buf, K_FOREVER) == 0){     // Get received data from queue
            // Handle receive data here.
            if(instance.HandleReceivedData(rx_buf) != DSMR::P1MonitorErrors::None){       // Forwarde to data handler
                LOG_ERR("(%s,%d) Unable handle received message", __func__, __LINE__);
            }
        }else{
            LOG_ERR("(%s,%d) Unable to retreive message", __func__, __LINE__);
        }
    }
};


void DSMR::uartP1MonitorISR(const struct device *unused, void *userData)
{
	uint8_t buf[32];
	int chunckLen;
	int i;
    auto& instance{DSMR::P1Monitor::GetInstance()};

	ARG_UNUSED(unused);
	ARG_UNUSED(userData);

	while (uart_irq_update(instance.mUart) &&
	       uart_irq_is_pending(instance.mUart)) {

        chunckLen = (!uart_irq_rx_ready(instance.mUart))? 0 : uart_fifo_read(instance.mUart, buf, sizeof(buf)); // Read butes from uart
		
        if (chunckLen == 0) {
			continue;
		}

		for (i = 0; i < chunckLen; i++) {
            k_timer_start(&instance.mRxTimer, K_MSEC(DSMR::P1Monitor::RECEIVE_TIMEOUT_MS), K_NO_WAIT);  // Reset timer to expire once after 100ms
            if(instance.mCurrentBuffer.length < sizeof(instance.mCurrentBuffer.data)){
                instance.mCurrentBuffer.data[instance.mCurrentBuffer.length++] = buf[i];    // add data to buffer
            }
            else{
                LOG_ERR("(%s,%d) RX buffer to small", __func__, __LINE__);
            }
		}
	}
}

DSMR::P1MonitorErrors DSMR::P1Monitor::SetupUart(){
    DSMR::P1MonitorErrors err{DSMR::P1MonitorErrors::None};
    uint8_t c{0};

    uart_irq_rx_disable(mUart);
	uart_irq_tx_disable(mUart);

	/* Drain the fifo */
	while (uart_fifo_read(mUart, &c, 1)) {
		continue;
	}

	uart_irq_callback_set(mUart, uartP1MonitorISR);
        
	uart_irq_rx_enable(mUart);

    return err;
}


DSMR::P1MonitorErrors DSMR::P1Monitor::Init(){
    printk("(%s, %d).\r\n", __func__, __LINE__);
    if(mInit){
        LOG_ERR("(%s, %d) P1 Monitor already initlialized.", __func__, __LINE__);
        return DSMR::P1MonitorErrors::None;
    }
        
    DSMR::P1MonitorErrors err{SetupUart()};
    if(err != DSMR::P1MonitorErrors::None){
        LOG_ERR("(%s, %d) Unable to initialize uart.", __func__, __LINE__);
        return err;
    }else{
        LOG_DBG("(%s, %d) UART init done...", __func__, __LINE__);
    }

    err = SetupGpio();
    if(err != DSMR::P1MonitorErrors::None){
        LOG_ERR("(%s, %d) Unable to initialize uart.", __func__, __LINE__);
        return err;
    }else{
        LOG_DBG("(%s, %d) GPIO init done...", __func__, __LINE__);
    }

    err = SetupTimer();
    if(err != DSMR::P1MonitorErrors::None){
        LOG_ERR("(%s, %d) Unable to initialize timer.", __func__, __LINE__);
        return err;
    }else{
        LOG_DBG("(%s, %d) TIMER init done...", __func__, __LINE__);
    }

    mInit = true;    // Flag that init has happened

    return err;
}

DSMR::P1MonitorErrors DSMR::P1Monitor::SetupGpio(){
    DSMR::P1MonitorErrors err{DSMR::P1MonitorErrors::None};
    if (!device_is_ready(mRTSPin.port)) {
		LOG_ERR("(%s, %d) Gpio device not ready.", __func__, __LINE__);
        err = DSMR::P1MonitorErrors::Failure;
	}

	if (gpio_pin_configure_dt(&mRTSPin, GPIO_OUTPUT_ACTIVE) < 0) {
        LOG_ERR("(%s, %d) Unable to set Gpio as output.", __func__, __LINE__);
        err = DSMR::P1MonitorErrors::Failure;
	}

    if (gpio_pin_set_dt(&mRTSPin, 1) < 0){
        LOG_ERR("(%s, %d) Unable to set Gpio high.",__func__, __LINE__);
        err = DSMR::P1MonitorErrors::Failure;
    }
    return err;
}

DSMR::P1MonitorErrors DSMR::P1Monitor::SetupTimer(){
    k_timer_init(&mRxTimer, DSMR::uartReceiveTimeout, NULL);
    return DSMR::P1MonitorErrors::None;
}

DSMR::P1MonitorErrors DSMR::P1Monitor::HandleReceivedData(const uart_dsmr_monitor_rx_buf& data){
    bool ret {mParser.Parse(data.data, data.length)};
    if(ret){
        if(mCallback)
            mCallback(mParser.GetMeasurments());
    }
    return ret ? DSMR::P1MonitorErrors::None : DSMR::P1MonitorErrors::Failure;
}


K_THREAD_DEFINE(dsmr_monitor_tid, DSMR::P1Monitor::P1MONITOR_THREAD_STACK_SIZE,
                DSMR::threadSerialDataHandler, NULL, NULL, NULL,
                DSMR::P1Monitor::P1MONTIOR_THREAD_PRIORITY, 0, 0);