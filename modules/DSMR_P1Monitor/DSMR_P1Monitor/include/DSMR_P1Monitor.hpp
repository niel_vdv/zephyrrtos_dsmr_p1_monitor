#ifndef _DSMR_MONITOR_HPP_
#define _DSMR_MONITOR_HPP_

#include "DSMR_P1Monitor_Types.hpp"
#include "DSMR_Parser.hpp"

#include "zephyr/device.h"
#include <zephyr/kernel.h>
#include <zephyr/drivers/gpio.h>

#if !DT_HAS_ALIAS_DSMR_SERIAL(DT_ALIAS(dsmr_p1monitor_serial))
#error Please define the alias dsmr-p1monitor-serial in the devicetree
#endif

#if !DT_HAD_NODELABEL_DSMR_P1_PIN(DT_NODELABEL(p1_pin))
#error Please define the p1_pin nodelabel in the devicetree
#endif

namespace DSMR{

typedef void (*MontitorCallback)(const OBISMeasurements& data);

/**
 * @brief Uart receive callback
 * 
 * @param unused 
 * @param userData 
 */
void uartP1MonitorISR(const struct device *unused, void *userData);
/**
 * @brief Thread to handle the received data
 * 
 */
void threadSerialDataHandler(void *, void *, void *);
/**
 * @brief Timer overflow ISR
 * 
 * @param timerId 
 */
void uartReceiveTimeout(struct k_timer *timerId);

class P1Monitor{
    friend void uartP1MonitorISR(const struct device *unused, void *user_data);
    friend void DSMR::threadSerialDataHandler(void *, void *, void *);
    friend void uartReceiveTimeout(struct k_timer *timer_id);
public:
    static constexpr uint8_t MSG_QUEUE_SIZE{10};                        // Message queue size
    static constexpr uint8_t P1MONTIOR_THREAD_PRIORITY{5};              // Monitor thread priority
    static constexpr uint16_t P1MONITOR_THREAD_STACK_SIZE{4096};        // Monitor thread stack size
    static constexpr uint16_t RECEIVE_TIMEOUT_MS{100};                  // Wait time after receiving byte from uart to start handling received data
    /**
     * @brief Get the object
     * 
     * @return P1Monitor& 
     */
    static P1Monitor& GetInstance(){
        static P1Monitor instance;
        return instance;
    }
    /**
     * @brief Init of the P1 port monitor
     * 
     * @return P1MonitorErrors 
     */
    P1MonitorErrors Init();
    /**
     * @brief Set the when data is received
     * 
     * @param callback 
     */
    void SetCallback(MontitorCallback callback){mCallback = callback;}

private:
    /**
     * @brief Construct a Monitor object
     * 
     */
    P1Monitor(){}
    /**
     * @brief Initialisation of the serial communication
     * 
     * @return P1MonitorErrors None on success
     */
    P1MonitorErrors SetupUart();
    /**
     * @brief Initialisation of the GPIO pin
     * 
     * @return P1MonitorErrors None on success
     */
    P1MonitorErrors SetupGpio();
    /**
     * @brief Initialisation of the timer
     * 
     * @return P1MonitorErrors None on success
     */
    P1MonitorErrors SetupTimer();
    /**
     * @brief Handle the received data from the serial interface
     * 
     * @param data received data buffer
     * @return P1MonitorErrors None on success
     */
    P1MonitorErrors HandleReceivedData(const uart_dsmr_monitor_rx_buf& data);
    

    Parser mParser{};                                                                   // Parser object
    const struct device *mUart{DEVICE_DT_GET(DT_ALIAS(dsmr_p1monitor_serial))};         // Uart device        
    const struct gpio_dt_spec mRTSPin = GPIO_DT_SPEC_GET(DT_NODELABEL(p1_pin), gpios);  // Gpio device
    uart_dsmr_monitor_rx_buf mCurrentBuffer;                                            // Receiving data buffer
    struct k_timer mRxTimer;                                                            // Timer for end of receive signaling
    bool mInit{false};                                                                  // Flag for init
    MontitorCallback mCallback{nullptr};                                                // Callback when receiving data
    
};


}

#endif