#ifndef __DSMR_MONITOR_HPP_
#define __DSMR_MONITOR_HPP_

#include <stdint.h>
#include <stddef.h>

#define DT_HAS_ALIAS_DSMR_SERIAL(prop) IS_ENABLED(DT_CAT(prop, _EXISTS))
#define DT_HAD_NODELABEL_DSMR_P1_PIN(prop) IS_ENABLED(DT_CAT(prop, _EXISTS))

namespace DSMR{

enum class P1MonitorErrors: uint8_t{
    None = 0,
    Failure,
};


struct uart_dsmr_monitor_rx_buf{
    uint8_t data[2048] = {0};
    size_t length{0};
};

}



#endif