#ifndef _DSMR_PARSER_HPP_
#define _DSMR_PARSER_HPP_

#include <stdlib.h>
#include <zephyr/kernel.h>
#include <string.h>

namespace DSMR{

constexpr uint8_t OBIS_REF_MAX_LEN{15};     // Max length of obis code
constexpr uint8_t OBIS_SN_MAX_LEN{30};      // Max length of Serial number

typedef char OBIS_REF_VAL[OBIS_REF_MAX_LEN];
typedef uint8_t OBIS_REF_IDX;
typedef char OBIS_SN[OBIS_SN_MAX_LEN];


/**
 * @brief Struct for dictionary
 * 
 */
struct OBISRefInfo{
    OBIS_REF_VAL code;
    OBIS_REF_IDX idx;
};

/**
 * @brief Struct for received time stamp
 * 
 */
struct OBISTimestamp {
    int year;
    int month;
    int day;
    int hour;
    int minute;
    int second;
};

struct OBISMaxDemand13Months{
    float data[13];
    uint8_t len;
};

/**
 * @brief Indexes of obis codes for parsing use. 
 *      
 *      ADD new idexes here if needed
 * 
 */
constexpr OBIS_REF_IDX OBIS_ID_IDX{0};
constexpr OBIS_REF_IDX OBIS_SN_IDX{1};
constexpr OBIS_REF_IDX OBIS_TIMESTAMP_IDX{2};
constexpr OBIS_REF_IDX OBIS_TOTAL_CONSUMPTION_RATE_1_IDX{3};
constexpr OBIS_REF_IDX OBIS_TOTAL_CONSUMPTION_RATE_2_IDX{4};
constexpr OBIS_REF_IDX OBIS_TOTAL_PRODUCTION_RATE_1_IDX{5};
constexpr OBIS_REF_IDX OBIS_TOTAL_PRODUCTION_RATE_2_IDX{6};
constexpr OBIS_REF_IDX OBIS_CURRENT_RATE_IDX{7};
constexpr OBIS_REF_IDX OBIS_ALL_PHASE_CONSUMPTION_IDX{8};
constexpr OBIS_REF_IDX OBIS_ALL_PHASE_PRODUCTION_IDX{9};
constexpr OBIS_REF_IDX OBIS_L1_PHASE_CONSUMPTION_IDX{10};
constexpr OBIS_REF_IDX OBIS_L2_PHASE_CONSUMPTION_IDX{11};
constexpr OBIS_REF_IDX OBIS_L3_PHASE_CONSUMPTION_IDX{12};
constexpr OBIS_REF_IDX OBIS_L1_PHASE_PRODUCTION_IDX{13};
constexpr OBIS_REF_IDX OBIS_L2_PHASE_PRODUCTION_IDX{14};
constexpr OBIS_REF_IDX OBIS_L3_PHASE_PRODUCTION_IDX{15};
constexpr OBIS_REF_IDX OBIS_L1_VOLTAGE_IDX{16};
constexpr OBIS_REF_IDX OBIS_L2_VOLTAGE_IDX{17};
constexpr OBIS_REF_IDX OBIS_L3_VOLTAGE_IDX{18};
constexpr OBIS_REF_IDX OBIS_L1_CURRENT_IDX{19};
constexpr OBIS_REF_IDX OBIS_L2_CURRENT_IDX{20};
constexpr OBIS_REF_IDX OBIS_L3_CURRENT_IDX{21};
constexpr OBIS_REF_IDX OBIS_BREAKER_STATE_IDX{22};
constexpr OBIS_REF_IDX OBIS_MAX_ALLOWED_POWER_IDX{23};
constexpr OBIS_REF_IDX OBIS_MAX_ALLOWED_CURRENT_IDX{24};
constexpr OBIS_REF_IDX OBIS_CURRENT_AVERAGE_IDX{25};
constexpr OBIS_REF_IDX OBIS_MAX_CONSUMPTION_CURRENT_MONTH_IDX{26};
constexpr OBIS_REF_IDX OBIS_MAX_CONSUMPTION_LAST_13_MONTHS_IDX{27};
constexpr OBIS_REF_IDX OBIS_TEXT_MESSAGE_IDX{28};
constexpr OBIS_REF_IDX OBIS_NOT_FOUND{255};

/**
 * @brief Dictionary like look up list for OBIS codes
 * 
 *      ADD new obis codes here if needed
 * 
 */
const OBISRefInfo obis_code_list[] = {
    {{"0-0:96.1.4"}, OBIS_ID_IDX},
    {{"0-0:96.1.1"}, OBIS_SN_IDX},
    {{"0-0:1.0.0"}, OBIS_TIMESTAMP_IDX},
    {{"1-0:1.8.1"}, OBIS_TOTAL_CONSUMPTION_RATE_1_IDX},
    {{"1-0:1.8.2"}, OBIS_TOTAL_CONSUMPTION_RATE_2_IDX},
    {{"1-0:2.8.1"}, OBIS_TOTAL_PRODUCTION_RATE_1_IDX},
    {{"1-0:2.8.2"}, OBIS_TOTAL_PRODUCTION_RATE_2_IDX},
    {{"0-0:96.14.0"}, OBIS_CURRENT_RATE_IDX},
    {{"1-0:1.7.0"}, OBIS_ALL_PHASE_CONSUMPTION_IDX},
    {{"1-0:2.7.0"}, OBIS_ALL_PHASE_PRODUCTION_IDX},
    {{"1-0:21.7.0"}, OBIS_L1_PHASE_CONSUMPTION_IDX},
    {{"1-0:41.7.0"}, OBIS_L2_PHASE_CONSUMPTION_IDX},
    {{"1-0:61.7.0"}, OBIS_L3_PHASE_CONSUMPTION_IDX},
    {{"1-0:22.7.0"}, OBIS_L1_PHASE_PRODUCTION_IDX},
    {{"1-0:42.7.0"}, OBIS_L2_PHASE_PRODUCTION_IDX},
    {{"1-0:62.7.0"}, OBIS_L3_PHASE_PRODUCTION_IDX},
    {{"1-0:32.7.0"}, OBIS_L1_VOLTAGE_IDX},
    {{"1-0:52.7.0"}, OBIS_L2_VOLTAGE_IDX},
    {{"1-0:72.7.0"}, OBIS_L3_VOLTAGE_IDX},
    {{"1-0:31.7.0"}, OBIS_L1_CURRENT_IDX},
    {{"1-0:51.7.0"}, OBIS_L2_CURRENT_IDX},
    {{"1-0:71.7.0"}, OBIS_L3_CURRENT_IDX},
    {{"0-0:96.3.10"}, OBIS_BREAKER_STATE_IDX},
    {{"0-0:17.0.0"}, OBIS_MAX_ALLOWED_POWER_IDX},
    {{"1-0:31.4.0"}, OBIS_MAX_ALLOWED_CURRENT_IDX},
    {{"1-0:1.4.0"}, OBIS_CURRENT_AVERAGE_IDX},
    {{"1-0:1.6.0"}, OBIS_MAX_CONSUMPTION_CURRENT_MONTH_IDX},
    {{"0-0:98.1.0"}, OBIS_MAX_CONSUMPTION_LAST_13_MONTHS_IDX},
    {{"0-0:96.13.0"}, OBIS_TEXT_MESSAGE_IDX},
};
/**
 * @brief Meter related received data
 * 
 */
struct OBISMeasurementsDevice{
    uint32_t ID;
    uint8_t rate;
    uint8_t breaker;
    OBIS_SN serialnumber;
};
/**
 * @brief Total power related received data
 * 
 */
struct OBISTotalPower{
    float consumptionRate1;
    float consumptionRate2;
    float productionRate1;
    float productionRate2;
};
/**
 * @brief Current power production related received data
 * 
 */
struct OBISCurrentPowerProduction{
    float all;
    float L1;
    float L2;
    float L3;
};
/**
 * @brief Current power consumption related received data
 * 
 */
struct OBISCurrentPowerConsumption{
    float all;
    float L1;
    float L2;
    float L3;
};
/**
 * @brief Average power related received data
 * 
 */
struct OBISAveragePower{
    float current;
    float maximum;
    OBISMaxDemand13Months demand;
    float maximum_allowed;
};
/**
 * @brief Voltage related received data
 * 
 */
struct OBISVoltage{
    float L1;
    float L2; 
    float L3;
};
/**
 * @brief Current related received data
 * 
 */
struct OBISCurrent{
    float L1;
    float L2;
    float L3;
    float maximum_allowed;
};
/**
 * @brief Collection of all received data
 * 
 */
struct OBISMeasurements{
    OBISTimestamp stamp;
    OBISMeasurementsDevice dev;
    OBISTotalPower powers;
    OBISAveragePower averages;
    OBISVoltage voltages;
    OBISCurrent currents;
    OBISCurrentPowerConsumption consumptions;
    OBISCurrentPowerProduction productions;
};
/**
 * @brief DSMR P1 Port data parser
 * 
 */
class Parser{
private:

    OBISMeasurements mMeasurements{0};  // Parsed data result
    /**
     * @brief Calculate CRC of received data
     * 
     * @param data Received data of P1 port
     * @param len Lenght of received data
     * @return uint16_t Calculated CRC of data
     */
    uint16_t CalculateCrc(const uint8_t* data, const size_t& len);
    /**
     * @brief Fetch the received CRC of P1 port
     * 
     * @param data Received data of P1 port
     * @param len Lenght of received data
     * @return uint16_t CRC16 value or 0 on failure
     */
    uint16_t FetchCrc(const uint8_t* data, const size_t& len);
    /**
     * @brief fetch float value of obis code line
     * 
     * @param input Received obis code line
     * @return float Parsed float value
     */
    float extractFloatValue(const char* input);
    /**
     * @brief fetch int value of obis code line
     * 
     * @param input Received obis code line
     * @return int Parsed int value
     */
    int extractIntValue(const char* input);
    /**
     * @brief fetch timestamp value of obis code line
     * 
     * @param input Received obis code line
     * @param stamp Parsed timestamp value
     */
    void extractTimestamp(const char* input, OBISTimestamp& stamp) ;
    /**
     * @brief fetch serial number of obis code line
     * 
     * @param input Received obis code line
     * @param sn Parsed serial number
     */
    void extractSerialNumber(const char* input, char* sn);
    /**
     * @brief fetch Max demand of the last 13 months
     * 
     * @param input Received obis code line
     * @param demand Parsed data
     */
    void extractLast13Months(const char *input, DSMR::OBISMaxDemand13Months &demand);
    /**
     * @brief Find index of received obis code line
     * 
     * @param line Received obis code line
     * @return OBIS_REF_IDX OBIS code index
     */
    OBIS_REF_IDX FindIdx(const char* line);
    /**
     * @brief Parse values out of obis lines
     * 
     * @param line Received obis code line
     * @param idx Obis code index
     * @return true on success
     * @return false on failure
     */
    bool DataParse(const char* line, const OBIS_REF_IDX& idx);
    /**
     * @brief Function to loop over all received lines of the received data
     * 
     * @param data Input data coming from the P1 monitor
     * @param len number of bytes received from P1 monitor
     * @return true on success
     * @return false on failure
     */
    bool LineParse(const uint8_t* data, const size_t& len);
public:
    /**
     * @brief Parse the received data buffer from the P1 monitor
     * 
     * @param data Received data from P1 monitor    
     * @param len Number of bytes received
     * @return true on success
     * @return false on failure
     */
    bool Parse(const uint8_t* data, const size_t& len);

    /**
     * @brief Get the parsed data result
     * 
     * @return const OBISMeasurements& 
     */
    const OBISMeasurements& GetMeasurments() const{return mMeasurements;}
};





}


#endif