#include <zephyr/kernel.h>
#include "DSMR_P1Monitor.hpp"
#include <zephyr/sys/printk.h>

 void receiveCallback(const DSMR::OBISMeasurements& data){
    printk("--- --- %02d:%02d:%02d --- ---\r\n", data.stamp.hour, data.stamp.minute, data.stamp.second);
    printk("Total power consumed (day): %.3fkWh\r\n", data.powers.consumptionRate1);
    printk("Total power produced (day): %.3fkWh\r\n", data.powers.productionRate1);
    printk("Current average power: %.3fkW\r\n", data.averages.current);
    printk("Max average power: %.3fkW\r\n", data.averages.maximum);
    printk("Total production: %.3fkW\r\n", data.productions.all);
    printk("L1 production: %.3fkW\r\n", data.productions.L1);
    printk("L2 production: %.3fkW\r\n", data.productions.L2);
    printk("L3 production: %.3fkW\r\n", data.productions.L3);
    printk("Total consumption: %.3fkW\r\n", data.consumptions.all);
    printk("\r\n");
 }

int main(){
    auto& instance = DSMR::P1Monitor::GetInstance();            // GetInstance of monitor
    DSMR::P1MonitorErrors err{DSMR::P1MonitorErrors::None};
    err = instance.Init();                                      // Init of monitor
    if(err != DSMR::P1MonitorErrors::None){                     // Check if init is okay
        printk("Init failed...\r\n");
    }
    instance.SetCallback(receiveCallback);                      // Set callback for received data.
    for(;;){
        printk(".\r\n");
        k_msleep(2000);
    }
    return 0;
}