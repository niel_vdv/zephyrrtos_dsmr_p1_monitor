# DSMR P1 Monitor module for Zephyr RTOS (version 3.5.0)
This Zephyr RTOS module provides a flexible and efficient solution for monitoring data output from Belgian digital meters. Designed for home automation projects, the module captures and processes data from these meters, offering users insights into their energy consumption.

# How to install
## 1. Copy Modules
Copy the modules folder to the root directory of your Zephyr RTOS build tree or added to the modules folder if you already created one.

## 2. Update CMake
In the CMake file of your application, include the following line:
```
include("${CMAKE_CURRENT_SOURCE_DIR}/../modules/modules.cmake")
```
This ensures proper integration of the required modules into your project.

# Devicetree entry
On overlay is needed to get a serial port and a GPIO pin. Some overlays are included in the example but the usage is straight forward. Fill in the ... with the GPIO pin and serial port you want to use. Make sure that the 'status' property of the chosen serial port is 'okay'. 

```
/ {
	p1_port_rx_enable {
		compatible = "gpio-leds";
		p1_pin: p1_pin {
			gpios = <...>;
			label = "P1 port RTS pin enable";
		};
	};

	aliases {
		dsmr-p1monitor-serial = &...;
	};
};

```

# Hardware setup
Connect the p1_pin device tree nodelabel gpio pin with pin 2 (data request) of the RJ12 connector going to the P1 port of the digital meter. Pin 5 (data) is an open collector pin, this needs to be pulled up with a resistor to the voltage which is supported by your MCU. In my case the data signal is inverted so I had to invert it. This can be done with a simple NPN or PNP BJT. Offcourse also connect the pin 3 (ground) to ground of your MCU.

# Configs
Required configs to get the main working. In the example I've used SEGGER RTT to get faster logs but it's not needed. 
```
CONFIG_CPLUSPLUS=y
CONFIG_STD_CPP20=y
CONFIG_LIB_CPLUSPLUS=y
CONFIG_NEWLIB_LIBC=y
CONFIG_NEWLIB_LIBC_NANO=y
CONFIG_ZERO_LATENCY_IRQS=y

CONFIG_SERIAL=y
CONFIG_UART_INTERRUPT_DRIVEN=y
CONFIG_UART_LINE_CTRL=y

CONFIG_LOG=y
CONFIG_LOG_MODE_MINIMAL=y
CONFIG_CBPRINTF_FP_SUPPORT=y

CONFIG_DSMR_P1MONITOR=y
CONFIG_CRC=y
CONFIG_MAIN_STACK_SIZE=2048
```



# Benefits of Lazy Parsing
While the parser class might be considered "lazy" in its current state, this intentional design choice aims to prioritize simplicity and ease of customization. Feel free to enhance and optimize the parser based on your specific needs.

# References
https://www.netbeheernederland.nl/_upload/Files/Slimme_meter_15_a727fce1f1.pdf

https://jensd.be/1205/linux/data-lezen-van-de-belgische-digitale-meter-met-de-p1-poort